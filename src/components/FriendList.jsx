import React, { Component, PropTypes } from 'react';


// import styles from './FriendList.css';
import FriendListItem from './FriendListItem';

export default class FriendList extends Component {
  constructor(props){
    super(props);
  }

  static propTypes = {
    friends: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
  }

  render () {

    var friendsArray =  Object.keys(this.props.friends).map((k) => this.props.friends[k]);
    var friendsList = friendsArray.map((friend) => {
              return (<FriendListItem
                key={friend.id}
                id={friend.id}
                name={friend.name}
                starred={friend.starred}
                {...this.props.actions} />);
            })  

    return (
      <ul>
        {friendsList}
      </ul>
    );
  }

}
