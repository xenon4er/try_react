import React, { Component, PropTypes } from 'react';
// import classnames from 'classnames';
// import styles from './FriendListItem.css';

export default class FriendListItem extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    starred: PropTypes.bool,
    starFriend: PropTypes.func.isRequired,
    deleteFriend: PropTypes.func.isRequired
  }

  render () {
    return (
      <li >
        <div>
          <div><span>{this.props.name}</span></div>
          <div><small>xx friends in common</small></div>
        </div>
        <div >
          <button className={`btn btn-default`} onClick={() => this.props.starFriend(this.props.id)}>
           {this.props.starred ? 'unstar' : 'star' }
          </button>
          <button className={`btn btn-default`} onClick={() => this.props.deleteFriend(this.props.id)}>
            delete
          </button>
        </div>
      </li>
    );
  }

}