import React, { Component, PropTypes } from 'react';
// import styles from './FriendListApp.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as FriendsActions from '../actions/FriendsActions';
import { FriendList, AddFriendInput } from '../components';

// @connect(state => ({
//   friendlist: state.friendlist
// }))


export class FriendListApp extends Component {
  constructor(props){
    super(props);
  }

  static propTypes = {
    friendsById: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  }
  render () {
    const { friendlist: { friendsById }, dispatch } = this.props;
    const actions = bindActionCreators(FriendsActions, this.props.dispatch);

    return (
      <div>
        <h1>The FriendList2</h1>
        <AddFriendInput addFriend={actions.addFriend} />
        <FriendList friends={friendsById} actions={actions} />
      </div>
    );
  }
}


function mapStateToProps (state) {
  console.log(state);
	const { friendlist: { friendsById }, dispatch } = state;
  return {
    friendlist: state.friendlist
  }
}

export default connect(mapStateToProps)(FriendListApp)