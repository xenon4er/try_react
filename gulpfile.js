'use strict';
var gulp = require('gulp'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    babelify = require('babelify'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload;
    
    // sass = require('gulp-sass'),
    // sourcemaps = require('gulp-sourcemaps'),
    // watch = require('gulp-watch'),
    // prefix = require('gulp-autoprefixer'),
     // babel = require("gulp-babel");
var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
        css: 'sites/all/themes/professional_theme/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
        style: 'sites/all/themes/professional_theme/main.scss',
        img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'sites/all/themes/professional_theme/templates/**/*.tpl.php',
        js: 'src/js/**/*.js',
        style: 'sites/all/themes/professional_theme/**/*.scss',
        css: 'sites/all/themes/professional_theme/**/*.css',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};
/*gulp.task('myStyles', function () {
    gulp.src(path.watch.css)
      // .pipe(livereload());
    //.pipe(browserSync.reload());
    .pipe(browserSync.stream());
});
*/
/*gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass({
                outputStyle: 'expanded'
            }
        )) //Скомпилируем
        .pipe(prefix()) //Добавим вендорные префиксы
        //.pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        //.pipe(reload({stream: true}));
});
*/
/*gulp.task('watch', function(){
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
});
*/
// gulp.task('webserver', function () {
//     browserSync.init({
//         proxy: "localhost"
//     });
//      gulp.watch(path.watch.css, ['myStyles']);   
//       gulp.watch(path.watch.html).on('change', browserSync.reload); 
// });
gulp.task('build', () => {
    return browserify({
            entries: './src/index.jsx',
            extensions: ['.jsx', '.js'],
            debug: true
        })
        .transform(babelify)
        .bundle()
        // .on('error', function(err){
        //     gutil.log(gutil.colors.red.bold('[browserify error]'));
        //     gutil.log(err.message);
        //     this.emit('end');
        // })
        .pipe(source('index.js'))
        .pipe(gulp.dest('dist'))
        .pipe(reload({stream: true}));
});
gulp.task('watch', ['build'], function () {
    gulp.watch('src/**/*.jsx', ['build']);
});
gulp.task('webserver', function() {
    browserSync.init({
        server: {
            baseDir: "./",
            open: false
        }
    });
});
gulp.task('default', ['webserver','watch']);